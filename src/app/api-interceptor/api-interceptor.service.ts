import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiInterceptorService implements HttpInterceptor {
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const matches = req.url.match(/^api:\/\//);
    if (matches !== null) {
      req = req.clone({
        url: req.url.replace(matches[0], `${environment.baseUrls.api}`)
      });
    }
    return next.handle(req);
  }
}
