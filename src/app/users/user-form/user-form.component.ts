import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user-service/user-service.service';
import { User } from 'src/models/user.model';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'atos-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _userService: UserService,
    private readonly _dialogRef: MatDialogRef<UserFormComponent>
  ) { }

  ngOnInit() {
    this._createForm();
  }

  public submit() {
    if (this.form.invalid) {
      return;
    }
    const u = new User();
    u.firstName = this.form.value.firstName;
    u.lastName = this.form.value.lastName;
    u.email = this.form.value.email;
    u.avatar = this.form.value.avatar;
    this._userService.addUser(u);
    this._dialogRef.close();
  }

  private _createForm() {
    this.form = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      avatar: ['']
    });
  }
}
