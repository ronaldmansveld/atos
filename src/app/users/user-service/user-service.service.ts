import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/models/user.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { UserListDto } from 'src/models/user-list.dto';
import { map, tap, switchMap } from 'rxjs/operators';
import { UserDto } from 'src/models/user.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _users: User[] = [];

  private _users$: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  public get users$(): Observable<User[]> {
    return this._users$.asObservable();
  }

  constructor(
    private readonly _http: HttpClient
  ) { }

  public loadUsers() {
    this._users = null;
    this._push();
    this._load().subscribe();
  }

  public deleteUser(id: number) {
    this._users = this._users.filter(user => user.id !== id);
    this._push();
    this._http.delete(`api://users/${id}`).subscribe();
  }

  public addUser(user: User) {
    this._http.post<UserDto>('api://users', user.toDto()).pipe(
      map(dto => User.fromDto(dto))
    ).subscribe(u => {
      this._users.push(u);
      this._push();
    });
  }

  private _load(page = 1): Observable<User[]> {
    return this._query(page).pipe(
      tap(users => {
        this._users = [...(this._users || []), ...users];
        this._push();
      }),
      switchMap(users => {
        if (users.length === 0) {
          return of(this._users);
        }
        return this._load(++page);
      })
    );
  }

  private _query(page = 1): Observable<User[]> {
    return this._http.get<UserListDto>(`api://users?page=${page}`).pipe(
      map(dto => dto.data),
      map(dtos => dtos.map(dto => User.fromDto(dto)))
    );
  }

  private _push() {
    this._users$.next(this._users);
  }

}
