import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/models/user.model';
import { UserService } from '../user-service/user-service.service';
import { MatDialog } from '@angular/material';
import { UserFormComponent } from '../user-form/user-form.component';

@Component({
  selector: 'atos-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public get users$(): Observable<User[]> {
    return this._userService.users$;
  }

  constructor(
    private readonly _userService: UserService,
    private readonly _dialog: MatDialog
  ) { }

  ngOnInit() {
    this._userService.loadUsers();
  }

  public remove(id: number) {
    this._userService.deleteUser(id);
  }

  public addUser() {
    const ref = this._dialog.open(UserFormComponent);
  }

}
