import { UserDto } from './user.dto';

export interface UserListDto {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: UserDto[];
}
