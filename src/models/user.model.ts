import { UserDto } from './user.dto';

export class User {
  public id: number;
  public email: string;
  public firstName: string;
  public lastName: string;
  public avatar: string;

  public get name(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  public static fromDto(dto: UserDto): User {
    const model = new User();
    model.id = dto.id;
    model.email = dto.email;
    model.firstName = dto.first_name;
    model.lastName = dto.last_name;
    model.avatar = dto.avatar;
    return model;
  }

  public toDto(): UserDto {
    return {
      id: this.id,
      email: this.email,
      first_name: this.firstName,
      last_name: this.lastName,
      avatar: this.avatar
    };
  }
}
