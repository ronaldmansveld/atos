## notes

- Not much work has been done on layout/design or animations
- No caching or robust error-handling has been implemented around API calls, as especially error-handling is dependant on UX requirements
- the use of the api:// scheme (and associated interceptor) allows to easily switch between different API urls for different environments
- I used a state-pattern (through exposing a users\$ observable from the service) to allow loading of all users as specified in the requirements, whereas the API returns paginated results. Depending on requirements and API design this may not always be the best solution.
- No tests have been written, as to keep spent time to a reasonable level22
